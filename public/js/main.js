let carts = document.querySelectorAll('.add-cart');
const cartBox = document.querySelector('.cartbox')
let products = [
    {
        name: 'Jordan 1 DM',
        tag: 'jordan0',
        price: 500,
        inCart: 0
    },
    {
        name: 'Jordan 1 Low',
        tag: 'jordan1',
        price: 600,
        inCart: 0
    },
    {
        name: 'Jordan 2',
        tag: 'jordan2',
        price: 400,
        inCart: 0
    },
    {
        name: 'Jordan 3',
        tag: 'jordan3',
        price: 550,
        inCart: 0
    },
    {
        name: 'Jordan 4',
        tag: 'jordan4',
        price: 600,
        inCart: 0
    },
    {
        name: 'Jordan 5',
        tag: 'jordan5',
        price: 650,
        inCart: 0
    },
    {
        name: 'Jordan 6',
        tag: 'jordan6',
        price: 450,
        inCart: 0
    },
    {
        name: 'Jordan 7',
        tag: 'jordan7',
        price: 675,
        inCart: 0
    },
    {
        name: 'Jordan 8',
        tag: 'jordan8',
        price: 700,
        inCart: 0
    }
];

for(let i=0; i < carts.length; i++){
    carts[i].addEventListener('click', () => {
        cartNumbers(products[i]);
        totalCost(products[i])
    })
}

function onLoadCartNumbers(){
    let productNumbers = localStorage.getItem('cartNumbers');

    if(productNumbers){
        document.querySelector('.navbar span').textContent = productNumbers;

    }
}

function cartNumbers(product) {
    let productNumbers = localStorage.getItem('cartNumbers');
    productNumbers = parseInt(productNumbers);

    if(productNumbers){
        localStorage.setItem('cartNumbers', productNumbers + 1);
        document.querySelector('.navbar span').textContent = productNumbers + 1;

    } else {
        localStorage.setItem('cartNumbers', 1);
        document.querySelector('.navbar span').textContent =1;
        
    }
    setItems(product)
}

function setItems(product){
    let cartItems = localStorage.getItem('productsInCart');
    cartItems = JSON.parse(cartItems);
    console.log(cartItems);

    if(cartItems != null){
        if(cartItems[product.name] == undefined){
            cartItems = {
                ...cartItems,
                [product.name]: product
            }
        }
        cartItems[product.name].inCart += 1;
    } else {
        product.inCart =1;
        cartItems = {
            [product.name] : product
        }
    }
    
    localStorage.setItem("productsInCart", JSON.stringify(cartItems));
}

function totalCost(product){
    let cartCost = localStorage.getItem('totalCost')

    if(cartCost != null){
        cartCost = parseInt(cartCost);
        localStorage.setItem("totalCost", cartCost + product.price);
    }else{
        localStorage.setItem("totalCost", product.price);

    }
}

function displayCart(){
    let cartItems = localStorage.getItem("productsInCart");
    cartItems = JSON.parse(cartItems);
    let productContainer = document.querySelector(".product-container");
    let overAllTotal = document.querySelector(".overAllTotal");
    let cartCost = localStorage.getItem('totalCost')


    if( cartItems && productContainer ){
        productContainer.innerHTML = '';
        Object.values(cartItems).map(item => {
            productContainer.innerHTML += `
            <div class = "row">
                <img src="./img/${item.tag}.png" width ="200" height ="auto"><br>
                <span>Name: ${item.name}</span><br>
                <span>Price: $${item.price}.00</span><br>
                <span>Quantity: ${item.inCart}</span>
                <span>Total: $${item.inCart * item.price}.00</span>
                <a href="" class="remove checkout"">remove</a>
                

            `;
         
        });
           productContainer.innerHTML += `"
            <div class = "basketTotalContainer">
                <h4 style="text-align:right">Overall Total</h4>
                <h3 style="text-align:right">$${cartCost}.00</h4>
                <a href="checkout.html" class="add-cart checkout"">Checkout</a>
            </div>
            `
    }
}

onLoadCartNumbers();
displayCart();
